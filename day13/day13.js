const fs = require('fs');
const lines = fs.readFileSync("day13.txt", "utf8").toString().split("\n")

let total = 0
const layers = []

for (const line of lines) {
   const [layer, depth] = line.split(": ").map(s => parseInt(s))
   if (layer % ((depth-1)*2) === 0) {total += layer*depth}
   layers.push([layer, depth])
}

console.log(total)

function d (delay) {
    for (const l of layers) {
        if ((delay + l[0]) % ((l[1]-1)*2) === 0) {return false}
    }
    return true
}

let c = -1
while (!d(c)) { c++ }
console.log(c)
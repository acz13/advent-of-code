#include <iostream>

#define STEP 386

// I solved part 1 with an uninteresting naive solution if you were wondering

int main() {
    int after_zero {0};

    for (int i {1}, cur_index {0}; i <= 50000000; i++) {
        cur_index = (cur_index + STEP + 1) % i;
        if (!cur_index) after_zero = i;
    }

    std::cout << after_zero << std::endl;
    return 0;
}

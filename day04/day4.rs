use std::iter::FromIterator;

fn main() {
    let input = include_str!("day4.txt");

    println!("star 1: {}", process1(&input));
    println!("star 2: {}", process2(&input));
}

fn process1(input: &str) -> usize {
    input.lines().into_iter().filter(|passphrase| {
        let mut split_passphrase: Vec<&str> = passphrase.split_whitespace().collect();
        split_passphrase.sort();
        let mut duplicates_removed = split_passphrase.clone();
        duplicates_removed.dedup();
        split_passphrase == duplicates_removed
    }).count()
}

fn process2(input: &str) -> usize {
    input.lines().into_iter().filter(|passphrase| {
        let mut passphrase_with_characters_sorted: Vec<String> = passphrase.split_whitespace().map(|element| {
            let mut chars: Vec<char> = element.chars().collect();
            chars.sort_by(|a, b| a.cmp(b));
            String::from_iter(chars)
        }).collect();
        passphrase_with_characters_sorted.sort();
        let mut duplicates_removed = passphrase_with_characters_sorted.clone();
        duplicates_removed.dedup();
        passphrase_with_characters_sorted == duplicates_removed
    }).count()
}

use std::io::{BufReader,BufRead};
use std::fs::File;


fn minmax<i32, I>(vals: I) -> Option(i32)
where
    I: Iterator<Item = i32>,
{
    Some(vals.max().unwrap()-vals.min().unwrap()) // Because .unwrap() is fun
}

fn main() {
    let file = File::open("day2.txt").unwrap();
    println!("{}", BufReader::new(file)
        .lines()
        .map(|line|
            minmax(line.unwrap() // I am not a Rustacean
                .split_whitespace()
                .map(|s| s.parse().unwrap())).unwrap()) // Definitely not
        .sum())
}
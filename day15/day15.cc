#include <iostream>
#include <bitset>

#define MOD 2147483647
#define A_FACTOR 16807
#define B_FACTOR 48271

typedef long long llong;
typedef std::bitset<16> bs16;

bs16 step(llong* gen, int mul, int divis) {
    do {
      *gen = (*gen * mul) % MOD;
    } while (*gen & divis);
    return bs16(*gen);
}

int main() { 
    llong *genA = new llong(516), *genB = new llong(190);
    int count1 = 0, count2 = 0;

    for (int i = 0; i < 4e7; i++) {
        if (step(genA, 16807, 0) == step(genB, 48271, 0)) {
            count1++;
        }
    }

    *genA = 516, *genB = 190;
    for (int i = 0; i < 5e6; i++) {
        if(step(genA, 16807, 3) == step(genB, 48271, 7)) {
            count2++;
        }
    }

    std::cout << count1 << " " << count2 << std::endl;
}
